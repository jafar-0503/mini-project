## Environment:
- Java version: 11
- Maven version: 3.*
- Spring Boot version: 2.7.8
- PostgreSQL version: 12
- Docker version: 20.10.21
- Docker Compose version: v2.13.0

## Requirements:
Below is the endpoint for the `GET` API endpoint for `Get All Data Employee`.

`GET /api/v1/employees`

For the `GET` API endpoint with a single path params `{id}` passed to the rest API.

`GET /api/v1/employees/{id}`

For the `GET` API endpoint with a single path params `{status}` `(aktif or nonaktif)` passed to the rest API.

`GET /api/v1/employees/status/{status}`

For the `POST` API endpoint add data `employee`.

`POST /api/v1/employees`

Request Body:
```json
{
  "first_name": "chris",
  "last_name": "jhon",
  "birth_date": "2021-04-15",
  "gender": "F",
  "status": "aktif",
  "hire_date": "2020-12-24"
}
```

For the `PUT` API endpoint edited data `employee`.

`PUT /api/v1/employees/{id}`

Request Body:
```json
{
  "first_name": "chris",
  "last_name": "jhon",
  "birth_date": "2021-04-15",
  "gender": "F",
  "status": "aktif",
  "hire_date": "2020-12-24"
}
```
For the `DEL` API endpoint remove data `employee` based on the value of the path param `{id}`.

`DEL /api/v1/employees/{id}`

Here is a series of requests and their corresponding expected responses:

`GET /api/v1/employees`:

Response Code: 200

Response Body:
```json
{
  "status": true,
  "data": [
    {
      "id": 1,
      "createdBy": "SYS",
      "from_date": "2023-02-11T17:04:35.681+00:00",
      "lastModifiedBy": "SYS",
      "to_date": "2023-02-11T17:04:35.681+00:00",
      "birth_date": "2021-04-15T00:00:00.000+00:00",
      "first_name": "chris",
      "last_name": "jhon",
      "gender": "F",
      "status": "aktif",
      "hire_date": "2020-12-24T00:00:00.000+00:00"
    },
    {
      "id": 2,
      "createdBy": "SYS",
      "from_date": "2023-02-12T00:16:34.910+00:00",
      "lastModifiedBy": "SYS",
      "to_date": "2023-02-12T00:16:34.910+00:00",
      "birth_date": "2001-02-13T00:00:00.000+00:00",
      "first_name": "jhon",
      "last_name": "doe",
      "gender": "F",
      "status": "aktif",
      "hire_date": "2020-12-24T00:00:00.000+00:00"
    }
  ],
  "message": "Employee retrieved successfully"
}
```

`GET /api/v1/employees/{id}`:

example: `GET /api/v1/employees/2`

Response Code: 200

Response Body:
```json
{
  "status": true,
  "data": {
    "id": 2,
    "createdBy": "SYS",
    "from_date": "2023-02-12T00:16:34.910+00:00",
    "lastModifiedBy": "SYS",
    "to_date": "2023-02-12T00:16:34.910+00:00",
    "birth_date": "2001-02-13T00:00:00.000+00:00",
    "first_name": "jhon",
    "last_name": "doe",
    "gender": "F",
    "status": "aktif",
    "hire_date": "2020-12-24T00:00:00.000+00:00"
  },
  "message": "Employee retrieved successfully"
}
```

`GET /api/v1/employees/status/{status}`:

Response Code: 200

Response Body:
```json
{
  "status": true,
  "data": [
    {
      "id": 1,
      "createdBy": "SYS",
      "from_date": "2023-02-11T17:04:35.681+00:00",
      "lastModifiedBy": "SYS",
      "to_date": "2023-02-11T17:04:35.681+00:00",
      "birth_date": "2021-04-15T00:00:00.000+00:00",
      "first_name": "chris",
      "last_name": "jhon",
      "gender": "F",
      "status": "aktif",
      "hire_date": "2020-12-24T00:00:00.000+00:00"
    },
    {
      "id": 2,
      "createdBy": "SYS",
      "from_date": "2023-02-12T00:16:34.910+00:00",
      "lastModifiedBy": "SYS",
      "to_date": "2023-02-12T00:16:34.910+00:00",
      "birth_date": "2001-02-13T00:00:00.000+00:00",
      "first_name": "jhon",
      "last_name": "doe",
      "gender": "F",
      "status": "aktif",
      "hire_date": "2020-12-24T00:00:00.000+00:00"
    }
  ],
  "message": "Employee Status retrieved successfully"
}
```

If data `(aktif or nonaktif)` is not yet added.

Response Body:
```json
{
  "status": false,
  "data": null,
  "message": "No Data"
}
```

`POST /api/v1/employees`:

Response Code: 200

Response Body:
```json
{
  "status": true,
  "data": {
    "id": 2,
    "createdBy": "SYS",
    "from_date": "2023-02-12T00:16:34.910+00:00",
    "lastModifiedBy": "SYS",
    "to_date": "2023-02-12T00:16:34.910+00:00",
    "birth_date": "2001-02-13T00:00:00.000+00:00",
    "first_name": "jhon",
    "last_name": "doe",
    "gender": "F",
    "status": "aktif",
    "hire_date": "2020-12-24T00:00:00.000+00:00"
  },
  "message": "Employee added successfully"
}
```

`PUT /api/v1/employees/{id}`:

example: `PUT /api/v1/employees/2`


Response Code: 200

Response Body:
```json
{
  "status": true,
  "data": {
    "id": 2,
    "createdBy": "SYS",
    "from_date": "2023-02-12T00:16:34.910+00:00",
    "lastModifiedBy": "SYS",
    "to_date": "2023-02-12T00:16:34.910+00:00",
    "birth_date": "2001-02-13T00:00:00.000+00:00",
    "first_name": "jhon",
    "last_name": "doe",
    "gender": "F",
    "status": "aktif",
    "hire_date": "2020-12-24T00:00:00.000+00:00"
  },
  "message": "Employee added successfully"
}
```

`DEL /api/v1/employees/{id}`:

example: `DEL /api/v1/employees/2`


Response Code: 200

Response Body:
```json
{
  "status": true,
  "data": {
    "id": 2,
    "createdBy": "SYS",
    "from_date": "2023-02-12T00:16:34.910+00:00",
    "lastModifiedBy": "SYS",
    "to_date": "2023-02-12T00:40:52.105+00:00",
    "birth_date": "1965-08-23T00:00:00.000+00:00",
    "first_name": "jhon",
    "last_name": "tidore",
    "gender": "F",
    "status": "aktif",
    "hire_date": "2001-12-24T00:00:00.000+00:00"
  },
  "message": "Employee deleted successfully"
}
```

## Commands
- Build Maven: 
```bash
mvn clean install -DskipTests
```
- Run PostgreSQL in Docker: 
```bash
docker run --name db-admin -p 5432:5432 -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=admin123 -d postgres:12
```
- Run spring-boot: 
```bash
mvn clean install spring-boot:run
```
- Run App in docker compose: 
```bash
docker-compose up --build
```

## Documentation
- Dump SQL:
```
https://drive.google.com/file/d/1Z9DbMoiSkAoZ-0rQguuAQBKAoaK2TRbB/view?usp=share_link
```

- Collection Postman:
```
https://drive.google.com/file/d/1P-uz_oiA-kGAz2Wq3xGJg-xMLrnT-2W9/view?usp=share_link
```