package stream;

import com.google.gson.Gson;
import org.apache.commons.io.*;
import org.modelmapper.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class MockData {
    public static List<Person> getPeople() throws IOException {
        InputStream inputStream = new FileInputStream(new File("src/main/resources/people.json"));
        String json = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        Type listType = new TypeToken<ArrayList<Person>>() {
        }.getType();
        return new Gson().fromJson(json, listType);
    }

}
