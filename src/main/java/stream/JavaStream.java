package stream;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class JavaStream {
    public static void main(String[] args) throws IOException {
        //Generating Streams
        List<Person> people = MockData.getPeople();
        List<Person> target = new ArrayList<>();
        int limit = 5;
        int counter = 0;
        for (Person person : people) {
            if (person.getAge() <= 20) {
                target.add(person);
                counter++;
                if (counter == limit) {
                    break;
                }
            }
        }
        target.forEach(System.out::println);

    }
}
