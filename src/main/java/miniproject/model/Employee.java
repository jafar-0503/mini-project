package miniproject.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import miniproject.auditable.Auditable;
import miniproject.property.Gender;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "employees")
public class Employee extends Auditable<String> {
    @Column(name = "birth_date")
    private Date birth_date;

    @Column(name = "first_name", length = 14)
    private String first_name;

    @Column(name = "last_name", length = 16)
    private String last_name;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "status", length = 10)
    private String status;

    @Column(name = "hire_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date hire_date;

}
