package ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Role obj = context.getBean("name", Role.class);
        obj.getName();
        Role obj1 = context.getBean("position", Role.class);
        obj1.getPosition();
    }
}
