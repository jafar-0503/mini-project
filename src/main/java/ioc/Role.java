package ioc;

public class Role {
    private String name;
    private String position;

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    public void getName() {
        System.out.println("My name is : " + name);
    }

    public void getPosition() {
        System.out.println("Position : " + position);
    }
}
