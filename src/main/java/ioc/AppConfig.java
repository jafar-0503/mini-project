package ioc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public Role helloWorld() {
        Role role = new Role();
        role.setName("name");
        role.setPosition("IT Java Developer");
        return role;
    }
}
