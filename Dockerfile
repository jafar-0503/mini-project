FROM openjdk:11-jdk-slim AS jdk
ADD target/mini-project-0.0.1-SNAPSHOT.jar mini-project-0.0.1-SNAPSHOT.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "mini-project-0.0.1-SNAPSHOT.jar"]